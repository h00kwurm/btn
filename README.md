A BTN Android App
==========


An app that takes advantage of the BTN API. The API is woefully one-direction as far as only being
able to _get_ data but not able to _send_ data to the server.


Changelog
---------
* 1-11-14: born

Libraries
---------
1. This app makes use of [loopj's android-async](http://loopj.com/android-async-http/) for the json-rpc access that is required.

License
---------
    h00kwurm (BTN username), h00kwurm@gmail.com, Copyright 2014
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software distributed under the License is
    distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    implied. See the License for the specific language governing permissions and limitations
    under the License.