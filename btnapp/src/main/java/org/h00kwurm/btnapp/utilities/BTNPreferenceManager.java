package org.h00kwurm.btnapp.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by anatraj on 1/12/14.
 */
public class BTNPreferenceManager {

    public static final String PREFERENCE_KEY_FILE = "org.h00kwurm.btnapp.prefs";
    public static final String[] PREFERENCE_KEYS = {"AK"};

    public static void saveAPIKey(Context ctx, String value)
    {
        savePreference(ctx, PREFERENCE_KEYS[0], value);
    }

    public static String getAPIKey(Context ctx)
    {
        SharedPreferences reader = ctx.getSharedPreferences(PREFERENCE_KEY_FILE, Context.MODE_PRIVATE);
        return reader.getString(PREFERENCE_KEYS[0], "");
    }

    public static void removeAPIKey(Context ctx)
    {
        removePreference(ctx, PREFERENCE_KEYS[0]);
    }

    private static void savePreference(Context context, String key, String value)
    {
        SharedPreferences reader = context.getSharedPreferences(PREFERENCE_KEY_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = reader.edit();
        writer.putString(key, value);
        writer.commit();
    }

    private static void removePreference(Context context, String key)
    {
        SharedPreferences reader = context.getSharedPreferences(PREFERENCE_KEY_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = reader.edit();
        writer.remove(key);
        writer.commit();
    }

}
