package org.h00kwurm.btnapp.services;

import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.h00kwurm.btnapp.activities.MainActivity;
import org.h00kwurm.btnapp.interfaces.IJSONObject;
import org.h00kwurm.btnapp.utilities.BTNCallback;
import org.h00kwurm.btnapp.utilities.BTNLog;
import org.h00kwurm.btnapp.utilities.BTNPreferenceManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Random;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class BTNService {

    private static final String BASEURL = "http://api.btnapps.net";
    private static final int IDRANGE = 65536;

    private MainActivity mActivity;
    private String mAPIKey;
    private Random mIDGenerator;

    public BTNService(MainActivity ctx)
    {
        this(ctx, BTNPreferenceManager.getAPIKey(ctx));
    }

    public BTNService(MainActivity ctx, String APIKey)
    {
        mActivity = ctx;
        mAPIKey = APIKey;
        mIDGenerator = new Random();
    }

    public void makeRequest(String method, String[] parameters, final IJSONObject outputObject, final BTNCallback callback)
    {
        (new AsyncHttpClient()).post(mActivity, BASEURL, getParameterEntity(method, null), "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                BTNLog.v("BTN/CF", "response: " + response.toString());

                JSONObject error = response.optJSONObject("error");
                if( error != null )
                {
                    Toast.makeText(mActivity, error.optString("message"), Toast.LENGTH_LONG).show();
                    mActivity.logout();
                }
                else
                {
                    JSONObject result = response.optJSONObject("result");
                    if (result != null) {
                        outputObject.updateFromJson(result);
                        callback.next();
                    }
                }
            }

        });
    }

    private StringEntity getParameterEntity(String method, String[] parameters)
    {
        JSONObject output = new JSONObject();
        JSONArray paramArray = new JSONArray();

        try {
            output.put("method", method);

            paramArray.put(mAPIKey);
            if( parameters != null )
            {
                for( String param : parameters )
                    paramArray.put(param);
            }

            output.put("params", paramArray);

            output.put("id", mIDGenerator.nextInt(IDRANGE));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            return new StringEntity(output.toString());
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

}
