package org.h00kwurm.btnapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.h00kwurm.btnapp.R;
import org.h00kwurm.btnapp.utilities.BTNPreferenceManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by h00kwurm on 1/12/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class LoginActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if( BTNPreferenceManager.getAPIKey(this).isEmpty() )
        {
            final EditText mKeyField = ((EditText) findViewById(R.id.field_login_apikey));

            ((Button) findViewById(R.id.button_login_go)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String key = mKeyField.getText().toString();
                    String error = validateKey(key);
                    if( error.isEmpty() )
                    {
                        BTNPreferenceManager.saveAPIKey(LoginActivity.this, key);
                        enterApp();
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else
        {
            enterApp();
        }
    }

    private void enterApp()
    {
        Intent nextActivity = new Intent(this, MainActivity.class);
        startActivity(nextActivity);
        finish();
    }

    private String validateKey(String key)
    {
        if( key.length() == 0 )
            return "Please enter your BTN API key";

        if( key.length() != 32 )
            return "A BTN API key is 32 characters long";

        Pattern keyPattern = Pattern.compile("([abcdef0-9f]{32})");
        Matcher matcher = keyPattern.matcher(key);
        if( matcher.groupCount() == 0 )
            return "A BTN API key matches [abcdef0-9]{32}";

        return "";
    }
}
