package org.h00kwurm.btnapp.viewadapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.h00kwurm.btnapp.R;
import org.h00kwurm.btnapp.lists.PrivateMessageList;
import org.h00kwurm.btnapp.utilities.Computations;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class ExpandablePrivateMessageListAdapter extends BaseExpandableListAdapter {

    private PrivateMessageList mMessages;
    private Context mCurrentContext;

    public ExpandablePrivateMessageListAdapter(Context context, PrivateMessageList data) {
        mCurrentContext = context;
        mMessages = data;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mMessages.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mMessages;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mMessages.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mCurrentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.elv_user_privatemessages_header, parent, false);
            ((TextView) v.findViewById(R.id.field_privatemessages_group_number)).setText(String.format("%d | %d", mMessages.getNumberUnread(), mMessages.size()));
        }

        return v;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        View v = convertView;

        if( v == null )
        {
            LayoutInflater inflater = (LayoutInflater) mCurrentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.elv_user_privatemessages_item, parent, false);
            ((TextView)v.findViewById(R.id.label_privatemessages_item_username)).setText(mMessages.get(childPosition).getUsername());
            ((TextView)v.findViewById(R.id.label_privatemessages_item_subject)).setText(mMessages.get(childPosition).getSubject());
            ((TextView)v.findViewById(R.id.label_privatemessages_item_date)).setText(Computations.getTimeAgoFromTimeString(mMessages.get(childPosition).getDate()));

            if( mMessages.get(childPosition).isUnread() )
                ((RelativeLayout)v.findViewById(R.id.listitem_navbar_controller_item)).setBackgroundColor(Color.parseColor("#44373737"));
        }

        return v;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
