package org.h00kwurm.btnapp.btnmodels;

import org.h00kwurm.btnapp.interfaces.IJSONObject;
import org.json.JSONObject;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class BTNUser implements IJSONObject {

    private boolean mIsFilled = false;

    private Integer mClassLevel;
    private String mUsername;
    private Integer mSnatches;
    private String mTitle;
    private Integer mBonus;
    private Integer mLumens;
    private Integer mUploadsSnatched;
    private Integer mEnabled;
    private Integer mParanoia;
    private Integer mInvites;
    private Long mUpload;
    private Integer mHnR;
    private Long mJoinDate;
    private Integer mUserID;
    private Long mDownload;
    private String mClass;
    private String mEmail;

    public BTNUser()
    {
        mIsFilled = false;
    }

    public BTNUser(JSONObject temp)
    {
        this.updateFromJson(temp);
    }

    @Override
    public void updateFromJson(JSONObject temp)
    {
        setUsername(temp.optString("Username"));
        setEnabled(temp.optInt("Enabled"));
        setParanoia(temp.optInt("Paranoia"));
        setSnatches(temp.optInt("Snatches"));
        setUserID(temp.optInt("UserID"));
        setLumens(temp.optInt("Lumens"));
        setUpload(temp.optLong("Upload"));
        setTitle(temp.optString("Title"));
        setBonus(temp.optInt("Bonus"));
        setUploadsSnatched(temp.optInt("UploadsSnatched"));
        setInvites(temp.optInt("Invites"));
        setEmail(temp.optString("Email"));
        setHnR(temp.optInt("HnR"));
        setJoinDate(temp.optLong("JoinDate"));
        setUsername(temp.optString("Username"));
        setUserClass(temp.optString("Class"));
        setClassLevel(temp.optInt("ClassLevel"));
        setDownload(temp.optLong("Download"));
        mIsFilled = true;
    }

    /* Getters */

    public boolean getIsFilled()
    {
        return mIsFilled;
    }

    public Integer getClassLevel() {
        return mClassLevel;
    }

    public String getUsername() {
        return mUsername;
    }

    public Integer getSnatches() {
        return mSnatches;
    }

    public String getTitle() {
        return mTitle;
    }

    public Integer getBonus() {
        return mBonus;
    }

    public Integer getLumens() {
        return mLumens;
    }

    public Integer getUploadsSnatched() {
        return mUploadsSnatched;
    }

    public Integer getEnabled() {
        return mEnabled;
    }

    public Integer getParanoia() {
        return mParanoia;
    }

    public Integer getInvites() {
        return mInvites;
    }

    public Long getUpload() {
        return mUpload;
    }

    public Integer getHnR() {
        return mHnR;
    }

    public Long getJoinDate() {
        return mJoinDate;
    }

    public Integer getUserID() {
        return mUserID;
    }

    public Long getDownload() {
        return mDownload;
    }

    public String getUserClass() {
        return mClass;
    }

    public String getEmail() {
        return mEmail;
    }

    /* Setters */
    public void setClassLevel(Integer mClassLevel) {
        this.mClassLevel = mClassLevel;
    }

    public void setUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setSnatches(Integer mSnatches) {
        this.mSnatches = mSnatches;
    }

    public void setLumens(Integer mLumens) {
        this.mLumens = mLumens;
    }

    public void setBonus(Integer mBonus) {
        this.mBonus = mBonus;
    }

    public void setUploadsSnatched(Integer mUploadsSnatched) {
        this.mUploadsSnatched = mUploadsSnatched;
    }

    public void setEnabled(Integer mEnabled) {
        this.mEnabled = mEnabled;
    }

    public void setParanoia(Integer mParanoia) {
        this.mParanoia = mParanoia;
    }

    public void setUpload(Long mUpload) {
        this.mUpload = mUpload;
    }

    public void setHnR(Integer mHnR) {
        this.mHnR = mHnR;
    }

    public void setInvites(Integer mInvites) {
        this.mInvites = mInvites;
    }

    public void setJoinDate(Long mJoinDate) {
        this.mJoinDate = mJoinDate;
    }

    public void setUserID(Integer mUserID) {
        this.mUserID = mUserID;
    }

    public void setDownload(Long mDownload) {
        this.mDownload = mDownload;
    }

    public void setUserClass(String mClass) {
        this.mClass = mClass;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

}
