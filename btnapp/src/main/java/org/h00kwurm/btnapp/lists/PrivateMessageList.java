package org.h00kwurm.btnapp.lists;

import org.h00kwurm.btnapp.btnmodels.BTNPrivateMessage;
import org.h00kwurm.btnapp.interfaces.IJSONObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class PrivateMessageList extends ArrayList<BTNPrivateMessage> implements IJSONObject {

    private int mNumberUnread;
    private boolean mIsFilled = false;

    public PrivateMessageList()
    {
        mNumberUnread = 0;
        mIsFilled = false;
    }

    public PrivateMessageList(Collection<BTNPrivateMessage> list)
    {
        mNumberUnread = 0;
        updateFromCollection(list);
    }

    public boolean getIsFilled()
    {
        return mIsFilled;
    }

    private void updateFromCollection(Collection<BTNPrivateMessage> list) {
        Iterator itr = list.iterator();
        BTNPrivateMessage temp = null;

        while( itr.hasNext() )
        {
            temp = (BTNPrivateMessage) itr.next();
            add(temp);
        }
        mIsFilled = true;
    }

    public int getNumberUnread()
    {
        return mNumberUnread;
    }

    @Override
    public boolean add(BTNPrivateMessage object) {
        if( object.isUnread() )
            mNumberUnread++;
        return super.add(object);
    }

    @Override
    public void clear() {
        mNumberUnread = 0;
        super.clear();
    }

    @Override
    public boolean remove(Object object) {
        if( ((BTNPrivateMessage)object).isUnread() )
            mNumberUnread--;
        return super.remove(object);
    }

    @Override
    public void updateFromJson(JSONObject temp) {
        JSONArray tempResults = temp.optJSONArray("results");

        JSONObject tempObject;
        BTNPrivateMessage tempMessage;
        for(int i = 0; i < tempResults.length(); i++)
        {
            tempObject = tempResults.optJSONObject(i);
            tempMessage = new BTNPrivateMessage(tempObject);
            add(tempMessage);
        }
        mIsFilled = true;
    }
}
