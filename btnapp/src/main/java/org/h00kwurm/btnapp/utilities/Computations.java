package org.h00kwurm.btnapp.utilities;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Computations {

    private static final int BYTE_SCALE = 1024;
    private static final String[] BYTE_SCALE_SIZES = {"B", "KB", "MB", "GB", "TB", "PB", "EB"};

    private static final int[] TIME_SCALE = {1, 60, 3600, 86400, 604800, 2419200, 29030400, 290304000};
    private static final String[] TIME_SCALE_SIZES = {"sec", "min", "hrs", "days", "wks", "mos", "yrs", "decs"};

    public static String bytesToScaledString(Long bytes)
    {
        int times = 0;
        double temp = bytes;
        BTNLog.v("BTN/C: ", String.valueOf(temp));
        while( temp > BYTE_SCALE )
        {
            temp /= BYTE_SCALE;
            times++;
        }

        return String.format("%.0f %s", temp, BYTE_SCALE_SIZES[times]);
    }

    public static String getDateFromEpoch(Long secondsAfterEpoch)
    {
        return new java.text.SimpleDateFormat("MMM, yyyy").format(new java.util.Date(secondsAfterEpoch * 1000));
    }

    public static String getTimeAgoFromTimeString(Long pastTime)
    {
        long deltaTime = (System.currentTimeMillis()/1000) - pastTime;
        int index = 0;

        while( deltaTime > TIME_SCALE[index] && index < TIME_SCALE.length)
        {
            index++;
        }

        if( index > 0 )
            index--;
        float time = ((float)deltaTime)/TIME_SCALE[index];

        return String.format("%.1f %s ago", time, TIME_SCALE_SIZES[index]);
    }
}
