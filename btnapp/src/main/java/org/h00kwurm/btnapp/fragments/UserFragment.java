package org.h00kwurm.btnapp.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.h00kwurm.btnapp.R;
import org.h00kwurm.btnapp.activities.MainActivity;
import org.h00kwurm.btnapp.btnmodels.BTNUser;
import org.h00kwurm.btnapp.lists.PrivateMessageList;
import org.h00kwurm.btnapp.services.BTNService;
import org.h00kwurm.btnapp.utilities.BTNCallback;
import org.h00kwurm.btnapp.utilities.BTNLog;
import org.h00kwurm.btnapp.utilities.Computations;
import org.h00kwurm.btnapp.viewadapters.ExpandablePrivateMessageListAdapter;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class UserFragment extends Fragment {

    private BTNUser mUser;
    private PrivateMessageList mPrivateMessages;
    private ExpandablePrivateMessageListAdapter mPrivateMessagesAdapter;
    private ExpandableListView mPrivateMessageView;

    public UserFragment() {
        mUser = new BTNUser();
        mPrivateMessages = new PrivateMessageList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_user, container, false);

        final BTNService tempService = ((MainActivity)getActivity()).getBTNService();
        if( tempService != null )
        {
            if( !mUser.getIsFilled() )
                tempService.makeRequest("userInfo", null, mUser, new BTNCallback() {
                    @Override
                    public void next() {
                        ((TextView)rootView.findViewById(R.id.label_user_username)).setText(mUser.getUsername());
                        ((TextView)rootView.findViewById(R.id.label_user_email)).setText(mUser.getEmail());
                        ((TextView)rootView.findViewById(R.id.label_user_download)).setText(Computations.bytesToScaledString(mUser.getDownload()));
                        ((TextView)rootView.findViewById(R.id.label_user_upload)).setText(Computations.bytesToScaledString(mUser.getUpload()));
                    }
                });

            if( !mPrivateMessages.getIsFilled() )
                tempService.makeRequest("getInbox", null, mPrivateMessages, new BTNCallback() {
                    @Override
                    public void next() {
                        mPrivateMessageView = ((ExpandableListView)rootView.findViewById(R.id.list_user_privatemessages));
                        mPrivateMessagesAdapter = new ExpandablePrivateMessageListAdapter(getActivity(), mPrivateMessages);
                        mPrivateMessageView.setAdapter(mPrivateMessagesAdapter);
                    }
                });

        }
        else
        {
            BTNLog.v("BTN/UF", "the activity is null");
        }

        return rootView;
    }

}