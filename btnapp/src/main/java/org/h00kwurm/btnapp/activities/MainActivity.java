package org.h00kwurm.btnapp.activities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;

import org.h00kwurm.btnapp.R;
import org.h00kwurm.btnapp.fragments.UserFragment;
import org.h00kwurm.btnapp.services.BTNService;
import org.h00kwurm.btnapp.utilities.BTNLog;
import org.h00kwurm.btnapp.utilities.BTNPreferenceManager;
import org.h00kwurm.btnapp.views.NotificationView;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class MainActivity extends BaseActivity {

    private NotificationView mNotifier;
    private BTNService mBTNService;

    private ConnectivityBroadcastReceiver mConnectivityBroadcastReceiver = null;
    private boolean mReceiverRegistered = false;


    @Override
    int getActivityLayout() {
        return R.layout.activity_main;
    }

    @Override
    String getActivityTitle() {
        return "Broadcasthe.net";
    }

    @Override
    int getDrawerLayoutID() {
        return R.id.drawer_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String apikey = BTNPreferenceManager.getAPIKey(this);
        if( apikey.isEmpty() )
        {
            leaveApp();
        }

        mBTNService = new BTNService(this, apikey);
        mNotifier = (NotificationView) findViewById(R.id.view_notification);

        mConnectivityBroadcastReceiver = new ConnectivityBroadcastReceiver();
        IntentFilter connectionFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mConnectivityBroadcastReceiver, connectionFilter);
        mReceiverRegistered = true;

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.frame_content, new UserFragment())
                    .commit();
        }
    }

    public BTNService getBTNService()
    {
        return mBTNService;
    }

    public void logout()
    {
        BTNPreferenceManager.removeAPIKey(this);
        leaveApp();
    }

    public void leaveApp()
    {
        if( mConnectivityBroadcastReceiver != null && mReceiverRegistered )
        {
            unregisterReceiver(mConnectivityBroadcastReceiver);
            mReceiverRegistered = false;
        }
        Intent nextActivity = new Intent(this, LoginActivity.class);
        startActivity(nextActivity);
        finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                /* check for internet */
                if (!networkConnected())
                {
                    BTNLog.v("Diplo/DA/OC", "not connected to internet!");
                    mNotifier.postNotification("no more internetz");
                }
                else
                {
                    BTNLog.v("Diplo/DA/OC", "reconnected to the internet");
                    mNotifier.clearNotification();
                }
            }
        }
    }
}
