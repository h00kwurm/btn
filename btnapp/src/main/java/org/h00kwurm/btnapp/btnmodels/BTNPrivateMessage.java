package org.h00kwurm.btnapp.btnmodels;

import org.h00kwurm.btnapp.interfaces.IJSONObject;
import org.json.JSONObject;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class BTNPrivateMessage implements IJSONObject {

    private boolean mIsFilled = false;

    private Integer mID;
    private String mSubject;
    private boolean mUnread;
    private boolean mSticky;
    private Integer mUserID;
    private String mUsername;
    private Long mDate;

    public BTNPrivateMessage()
    {
        mIsFilled = false;
    }

    public BTNPrivateMessage(JSONObject temp)
    {
        updateFromJson(temp);
    }

    @Override
    public void updateFromJson(JSONObject temp) {
        setID(temp.optInt("ID"));
        setSubject(temp.optString("Subject"));
        setUnread(temp.optInt("Unread") == 1);
        setSticky(temp.optInt("Sticky") == 1);
        setUserID(temp.optInt("UserID"));
        String uname = temp.optString("Username");
        if( uname == null || uname.equals("null") )
        {
            setUsername("System");
        }
        else
        {
            setUsername(uname);
        }
        setDate(temp.optLong("Date"));
        mIsFilled = true;
    }

    public boolean getIsFilled()
    {
        return mIsFilled;
    }

    public Integer getID() {
        return mID;
    }

    public void setID(Integer mID) {
        this.mID = mID;
    }

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String mSubject) {
        this.mSubject = mSubject;
    }

    public boolean isUnread() {
        return mUnread;
    }

    public void setUnread(boolean mUnread) {
        this.mUnread = mUnread;
    }

    public boolean isSticky() {
        return mSticky;
    }

    public void setSticky(boolean mSticky) {
        this.mSticky = mSticky;
    }

    public Integer getUserID() {
        return mUserID;
    }

    public void setUserID(Integer mUserID) {
        this.mUserID = mUserID;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public Long getDate() {
        return mDate;
    }

    public void setDate(Long mDate) {
        this.mDate = mDate;
    }
}
