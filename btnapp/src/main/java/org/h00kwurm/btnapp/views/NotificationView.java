package org.h00kwurm.btnapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.h00kwurm.btnapp.R;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NotificationView extends RelativeLayout {

    private RelativeLayout mNotificationContainer;
    private Button mNotificationCancel;
    private TextView mNotificationText;
    private Runnable mNotificationRunnable;

    public NotificationView(Context context) {
        this(context, null);
    }

    public NotificationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_notification, this, true);
        initializeNotifier();
    }

    public void clearNotification()
    {
        if( mNotificationContainer.getVisibility() == View.VISIBLE )
        {
            mNotificationText.setText("");
            mNotificationRunnable = getDefaultRunnable();
            mNotificationContainer.setVisibility(View.INVISIBLE);
        }
    }

    public void postNotification(String notification)
    {
        mNotificationText.setText(notification);
        mNotificationContainer.setVisibility(View.VISIBLE);
        mNotificationRunnable = getDefaultRunnable();
    }

    public void postNotification(String notification, Runnable r)
    {
        mNotificationText.setText(notification);
        mNotificationContainer.setVisibility(View.VISIBLE);
        mNotificationRunnable = r;
    }

    private void initializeNotifier() {
        mNotificationContainer = (RelativeLayout)findViewById(R.id.container_notification);

        if( mNotificationRunnable == null )
        {
            mNotificationRunnable = getDefaultRunnable();
        }

        mNotificationText = (TextView)findViewById(R.id.label_notification_text);
        mNotificationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNotificationRunnable.run();
            }
        });

        mNotificationCancel = (Button)findViewById(R.id.button_notification_button);
        mNotificationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearNotification();
            }
        });
    }

    private Runnable getDefaultRunnable()
    {
        return new Runnable() {
            @Override
            public void run() {}
        };
    }
}
