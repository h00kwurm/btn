package org.h00kwurm.btnapp.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import org.h00kwurm.btnapp.R;

/**
 * Created by h00kwurm on 1/11/14.
 * Edited by:
 *  <insert your name here> <edit-year>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

public abstract class BaseActivity extends ActionBarActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;

    abstract int getActivityLayout();
    abstract String getActivityTitle();
    abstract int getDrawerLayoutID();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());

        mTitle = getActivityTitle();

        initNavDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initDrawerToggle();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(getActivityTitle());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void initNavDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(getDrawerLayoutID());
    }

    protected int getDrawerIcon() {
        return R.drawable.ic_nav_drawer;
    }

    private void initDrawerToggle()
    {
       mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, getDrawerIcon(), R.string.description_navbar_open, R.string.description_navbar_close){
           @Override
           public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
           }

           @Override
           public void onDrawerClosed(View drawerView) {
               invalidateOptionsMenu();
           }
       };
       mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public boolean networkConnected() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return ((activeNetwork != null) && (activeNetwork.isConnected()));
    }

}
